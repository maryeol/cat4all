package com.example.animalforall;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class thirdactivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView name, age, gender , adress ;
    private Button like , adopt , bckbtn;
    private AlertDialog.Builder alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thirdactivity);

        imageView = findViewById(R.id.image);
        name = findViewById(R.id.name);
        age = findViewById(R.id.age);
        gender = findViewById(R.id.gender);
        adress = findViewById(R.id.adress);

        like = findViewById(R.id.lkbtn);
        adopt = findViewById(R.id.adptbtn);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            String infokey = extras.getString("mykey");
            String NamefromSecondView = extras.getString("name_of_the_cat");
            String AgefromSecondView = extras.getString("age");
            String GenderfromSecondView = extras.getString("gender");
            String AdrfromSecondView = extras.getString("adress");

            showInfo(NamefromSecondView , AgefromSecondView , GenderfromSecondView , AdrfromSecondView
                    ,infokey );
        }


        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Toast.makeText(getApplicationContext(),"You liked this cat", Toast.LENGTH_SHORT).show();
            }
        });

        adopt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = new AlertDialog.Builder(thirdactivity.this);
                alertDialog.setTitle("Are you sure you want to adopt the cat ?");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton(R.string.postive_msg,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(), "You'll receive an email from " +
                                        "the owner <3 very soon ", Toast.LENGTH_LONG).show();
                            }

                        });


                alertDialog.setNegativeButton(R.string.negative_msg, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog dialog = alertDialog.create();
                dialog.show();

            }
        });

        bckbtn = findViewById(R.id.bckbtn);

        bckbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thirdactivity.this , secondactivity.class);
                startActivity(intent);
            }
        });

    }

    public void showInfo(String name1, String age1 ,String gender1, String adress1, String infokey){
        if (infokey.equals("catone")) {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.cat));
            name.setText(" name of the cat : " + name1);
            age.setText(" age : " + age1);
            gender.setText("gender : " + gender1);
            adress.setText(" adress : " + adress1);
        }
        else if (infokey.equals("cattwo")){
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.cat1));
            name.setText(" name of the cat : " + name1);
            age.setText(" age : " + age1);
            gender.setText("gender : " + gender1);
            adress.setText(" adress : " + adress1);
        }
    }


}

