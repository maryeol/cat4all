package com.example.animalforall;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class secondactivity extends AppCompatActivity implements View.OnClickListener {

    private Button button1 , button2 ;
    private ImageView catimg1 , catimg2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondactivity);
        button1 = findViewById(R.id.bckbtn);
        button2 = findViewById(R.id.notifbtn);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(secondactivity.this , MainActivity.class);
                startActivity(intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(secondactivity.this , Recycle.class);
                startActivity(intent);
            }
        });


        catimg1 = findViewById(R.id.img1);
        catimg2 = findViewById(R.id.img2);

        catimg1.setOnClickListener(this);
        catimg2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img1:{
                startActivity( new Intent(secondactivity.this , thirdactivity.class));
                Intent intent = new Intent(secondactivity.this , thirdactivity.class);
                intent.putExtra("mykey" , "catone");
                intent.putExtra("name_of_the_cat" , "Lolita");
                intent.putExtra("age" , "1 year");
                intent.putExtra("gender" , "female");
                intent.putExtra("adress" , "megrine");
                startActivity(intent);
            }
            break;
            case R.id.img2:{
                startActivity( new Intent(secondactivity.this , thirdactivity.class));
                Intent intent = new Intent(secondactivity.this , thirdactivity.class);
                intent.putExtra("mykey" , "cattwo");
                intent.putExtra("name_of_the_cat" , "Moon");
                intent.putExtra("age" , "2 years");
                intent.putExtra("gender" , "male");
                intent.putExtra("adress" , "megrine");
                startActivity(intent);
            }
            break;
        }
    }
}