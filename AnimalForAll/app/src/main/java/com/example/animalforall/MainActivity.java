package com.example.animalforall;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adapter.MyAdapter;
import model.Listitem;

public class MainActivity extends AppCompatActivity {



    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.btnstart);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this , secondactivity.class);
                startActivity(intent);
            }
        });
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        Toast.makeText(getApplicationContext()," On Start ", Toast.LENGTH_SHORT).show();
//
//    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        Toast.makeText(getApplicationContext()," On Stop ", Toast.LENGTH_SHORT).show();
//
//    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        Toast.makeText(getApplicationContext()," On Pause ", Toast.LENGTH_SHORT).show();
//
//    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        Toast.makeText(getApplicationContext()," On Rersume ", Toast.LENGTH_SHORT).show();
//
//    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        Toast.makeText(getApplicationContext()," On Destroy ", Toast.LENGTH_SHORT).show();
//
//    }

//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        Toast.makeText(getApplicationContext()," On Restart ", Toast.LENGTH_SHORT).show();
//
//    }
}